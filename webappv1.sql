CREATE DATABASE IF NOT EXISTS webappv1 DEFAULT CHARACTER SET latin1 COLLATE latin1_spanish_ci;
USE webappv1;


DROP TABLE IF EXISTS posts;
CREATE TABLE posts (
	id_post int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
	fc_post timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
	act_post tinyint(4) NOT NULL DEFAULT 1,
	nom_post varchar(512) CHARACTER SET utf8 COLLATE utf8_spanish_ci DEFAULT NULL,
	desc_post varchar(512) CHARACTER SET utf8 COLLATE utf8_spanish_ci DEFAULT NULL,
	img_post varchar(180) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL DEFAULT 'default_post.svg',
	slug_post varchar(120) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL DEFAULT 'none-s',
	type_post varchar(50) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL DEFAULT 'post'
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

INSERT INTO posts (id_post, fc_post, act_post, nom_post, desc_post, img_post, slug_post, type_post) VALUES
(1, '2023-09-14 06:00:00', 1, 'Instalar Linux sin Windows', NULL, 'default_post.svg', 'none-s', 'evt'),
(2, '2023-09-14 06:00:00', 1, 'Instalar Fedora dual boot con Windows', NULL, 'default_post.svg', 'none-s', 'evt'),
(3, '2023-09-14 06:00:00', 1, 'Buscador Aajax con MySQL y PHP', NULL, 'default_post.svg', 'none-s', 'evt'),
(4, '2023-09-14 06:00:00', 1, 'Compilar PHP 8.4', NULL, 'default_post.svg', 'none-s', 'evt'),
(5, '2023-09-14 06:00:00', 1, 'Configurar OBS en Fedor', NULL, 'default_post.svg', 'none-s', 'evt'),
(6, '2023-09-14 06:00:00', 1, '¿Qué hacer después de instalar Fedora 39?', NULL, 'default_post.svg', 'none-s', 'evt'),
(7, '2010-12-09 06:00:00', 1, 'Instalar XAMPP 8.2 en Fedora 38', NULL, 'default_post.svg', 'none-s', 'evt'),
(8, '2009-06-17 05:00:00', 1, 'Instalar MySQL 8 en Fedora 39', NULL, 'default_post.svg', 'none-s', 'evt'),
(9, '2023-09-07 06:00:00', 1, 'Un gestor de descargas de paquetes Yumex', 'uno', 'default_post.svg', 'none-s', 'fel'),
(10, '2023-09-14 06:00:00', 1, 'Actualizar Fedora 38 a 39', 'asas', 'default_post.svg', 'none-s', 'fel'),
(11, '2023-10-05 06:00:00', 1, 'Generar un QR en PHP', 'as', 'default_post.svg', 'none-s', 'fel'),
(12, '2023-10-05 06:00:00', 1, 'Instalar VirtualBox en Fedora', 'as', 'default_post.svg', 'none-s', 'fel'),
(13, '2023-10-19 06:00:00', 1, 'ZSH instalar en Fedora', 'Desde mundo malo', 'default_post.svg', 'none-s', 'fel'),
(14, '2023-10-06 06:00:00', 1, 'Tilix y ZSH instalar en Fedora', '_Uno cero desde antier_', 'default_post.svg', 'none-s', 'fel');
