<?php

namespace App\Controllers;
use App\Models\Post;

class Home extends BaseController
{
    protected $helpers = [];
    protected $db;
    protected $mPost;

    public function __construct(){
		helper(['url', 'session', 'email', 'upload', 'database', 'form']);
		$this->db = $db = \Config\Database::connect();
        $this->mPost = new Post();
	}

    public function index($page = 1)
	{
		$this->pager = \Config\Services::pager();
		$data = [
			'posts' => $this->mPost->select('posts.*')
				->orderBy('fc_post', 'desc')
				->paginate(4),
			'pager' => $this->pager,
			'title' => 'Inicio ' . env('appName'),
			'tab' => 'main',
		];
		return view('main', $data);
	}

    public function search()
	{
		$search = ($this->request->getVar('search')) ? $this->request->getVar('search') : "";
		$res['posts'] = $this->mPost->select('posts.*')
				->groupStart()
				->like("nom_post", $search, 'both')
				->groupEnd()
				->orderBy('fc_post', 'desc')
                ->paginate(5);
        return $this->response->setJSON($res);
	}
}
