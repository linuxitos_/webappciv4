<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="UTF-8">
		<meta name="description" content="The small framework with powerful features">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<link rel="icon" href="<?= base_url('public/assets/imgs/favicon.png'); ?>">
		<title><?= $title; ?></title>
		<!-- Bootstrap core CSS-->
		<link href="<?=base_url('public/assets/bootstrap/bootstrap.min.css');?>" rel="stylesheet">
		<link href="<?=base_url('public/assets/bootstrap-icons/bootstrap-icons.min.css');?>" rel="stylesheet">
		<link href="<?=base_url('public/assets/css/custom.css');?>" rel="stylesheet">

		<meta name="description" content="Tips sobre linux, Hacks, Tutoriales, Desarrollo Web e Ideas en un sólo Blog">
		<meta name="author" content="Fernando Merino">
		<meta name="keywords" content="linuxitos,fedora,development,blog,m2">

		<script src="<?= base_url('public/assets/jquery/jquery-3.7.1.min.js'); ?>"></script>
		<script src="<?= base_url('public/assets/bootstrap/bootstrap.bundle.min.js'); ?>"></script>
		<script src="<?= base_url('public/assets/js/ajx.js'); ?>"></script>
		<script type="text/javascript">
            var url = document.location.toString();
            var base_url = "";
		</script>
	</head>
	<body>
		<nav class="navbar navbar-expand-sm navbar-toggleable-sm navbar-dark bg-dark sticky-top">
            <div class="container">
                <button type="button" class="navbar-toggler text-dark" data-bs-toggle="offcanvas" data-bs-target="#offCanvasSearch" aria-controls="offCanvasSearch" title="Alternar navegación" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <a class="navbar-brand" runat="server" href="">
					<img class="img-fluid" src="<?= base_url('public/assets/imgs/logo.svg');?>" id="logo_custom" alt="logo">
				</a>

                <div class="collapse navbar-collapse d-sm-inline-flex justify-content-between">
                    <ul class="navbar-nav flex-grow-1">
                        <li class="nav-item"><a class="nav-link" runat="server" href="~/">Inicio</a></li>
                    </ul>
                </div>
                <form class="col-3 col-lg-3 lg-mb-3 mb-lg-0 me-lg-3 d-none" role="search">
                    <input type="search" class="form-control" placeholder="Búsqueda ..." aria-label="Search">
                </form>
                <div class="d-flex">
                    <button type="button" class="btn btn-link border-0 order-2 shadow-none openModal" data-bs-toggle="modal" data-bs-target="#mdlSearch" title="Alternar navegación" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
			            <i class="bi bi-search text-white bi-3x"></i>
                    </button>
                    <div class="dropdown text-end order-3 desplegable">
                        <a href="#" class="d-block link-body-emphasis text-decoration-none dropdown-toggle dropdown-menu-end text-white" data-bs-toggle="dropdown" aria-expanded="false">
                            <img src="<?= base_url('public/assets/imgs/user.png'); ?>" alt="mdo" width="32" height="32" class="rounded-circle">
							<span class="d-none d-md-inline-block text-white">Opciones</span>
                        </a>
                        <ul class="dropdown-menu text-small dropdown-menu-end desplegable-menu">
                            <li><a class="dropdown-item" href="/NewProject"><span class="d-inline-block bg-success rounded-circle p-1"></span> New project <i class="bi bi-plus-circle float-end"></i> </a></li>
                            <li><a class="dropdown-item" href="/Settings"><span class="d-inline-block bg-secondary rounded-circle p-1"></span> Settings <i class="bi bi-gear float-end"></i></a></li>
                            <li><a class="dropdown-item" href="/Profile"><span class="d-inline-block bg-primary rounded-circle p-1"></span> Profile <i class="bi bi-person-circle float-end"></i></a></li>
                            <li><a class="dropdown-item" href="/ScrollSpy"><span class="d-inline-block bg-warning rounded-circle p-1"></span> Docs <i class="bi bi-filetype-doc float-end"></i></a></li>
                            <li>
                                <a class="dropdown-item" href="/views/Tabs">
                                    <span class="d-inline-block bg-info rounded-circle p-1"></span> Tabs <i class="bi bi-sticky float-end"></i>
                                </a>
                            </li>
                            <li>
                                <a class="dropdown-item" href="/views/NavRight">
                                    <span class="d-inline-block bg-black rounded-circle p-1"></span> Offcanvas <i class="bi bi-caret-right-square float-end"></i>
                                </a>
                            </li>
                            <li><hr class="dropdown-divider"></li>
                            <li><a class="dropdown-item" href="#"><span class="d-inline-block bg-danger rounded-circle p-1"></span> Sign out <i class="bi bi-box-arrow-right float-end"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </nav>

        <div class="container mt-3">
            <div id="div-cnt-ajax" class="row">
                <?php
				foreach($posts as $post){
					echo '<div class="col-lg-4 col-md-4 col-sm-6 col-6 mb-3 d-flex">
							<div id="post-'.$post->id_post.'" class="card post border-bottom-2" style="width:100%;">
								<div class="card-img-top-new img-new">
									<a href="'.base_url('post/'.$post->nom_post).'" title="'.ucfirst($post->nom_post).'">
										<div class="post-image">
											<img src="'.base_url('public/assets/imgs/default_post.png').'" class="img-fluid">
										</div>
									</a>
								</div>
								<div class="card-body p-2">
									<div class="post-header">
										<h2 class="post-title mt-2">
											<a href="#" title="'.$post->nom_post.'">
												'.ucfirst($post->nom_post).'
											</a>
										</h2>
									</div>
								</div>
								<div class="card-footer p-3 bg-white">
									<p class="post-meta p-0 m-0">
										<a href="#">'.'</a>  <a class="float-end" href="">0 comentarios</a>
									</p>
								</div>
							</div>
					</div>';
				}
				?>

                <div class="col-md-12 mb-4">
					<div id="pag-temas">
						<?php
						if ($pager) {
							echo $pager->links('default', 'bootstrap_pagination');
						}
						?>
					</div>
				</div>
            </div>
        </div>

        <!-- Modal -->
        <div class="modal fade mt-5" id="mdlSearch" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog mt-8">
                <div class="modal-content">
                    <div class="modal-body">
						<form action="/" method="post" class="formSearch">
                            <div class="input-group">
                                <input runat="server" type="text" class="form-control form-control-lg border-dark border-end-0 shadow-none txtSearch"
                                    placeholder="Búsqueda" aria-label="Not" aria-describedby="basic-addon1" required="required" autocomplete="off">
                                <button runat="server" class="btn btn-link border-0 shadow-none border-top border-bottom border-end border-dark" type="submit" id="btnSearch">
                                    <i class="bi bi-search"></i>
                                </button>
                            </div>
						</form>
						<div class="row mt-2">
							<div class="col-md-12">
                                <div class="d-flex flex-column flex-md-row mt-2 align-items-center justify-content-center">
                                    <div id="divCnt" class="list-group w-100"></div>
                                </div>
							</div>
						</div>
                    </div>
                </div>
            </div>
        </div>

        <div class="offcanvas offcanvas-start" data-bs-scroll="true" tabindex="-1" id="offCanvasSearch" aria-labelledby="offcanvasWithBothOptionsLabel">
            <div class="offcanvas-header bg-dark">
                <a class="navbar-brand text-white" runat="server" href="~/">
                    <img src="<?= base_url('public/assets/imgs/logo.svg');?>" alt="mdo" width="100">
                </a>
                <h5 class="offcanvas-title" id="offcanvasWithBothOptionsLabel"></h5>
                <button type="button" class=" btn-close btn-close-white" data-bs-dismiss="offcanvas" aria-label="Close"></button>
            </div>
            <div class="offcanvas-body">
				<div class="row">
					<div class="col-md-12">
                        <div class=" w-100">
                            <a href="/" class="d-flex align-items-center mb-3 mb-md-0 me-md-auto text-decoration-none">
                                <i class="bi bi-ui-checks-grid"></i> <span class="fs-4 mx-3">Opciones</span>
                            </a>
                            <hr>
                            <ul class="nav nav-pills flex-column mb-auto">
                                <li class="nav-item">
                                <a href="#" class="nav-link active" aria-current="page">
                                    <i class="bi bi-house"></i> Inicio
                                </a>
                                </li>
                                <li>
                                <a href="#" class="nav-link">
                                    <i class="bi bi-person-vcard"></i> Mis posts
                                </a>
                                </li>
                                <li>
                                <a href="#" class="nav-link">
                                    <i class="bi bi-toggles"></i> Config
                                </a>
                                </li>
                                <li>
                                <a href="#" class="nav-link">
                                    <i class="bi bi-gear"></i> Cuenta
                                </a>
                                </li>
                                <li>
                                <a href="#" class="nav-link">
                                    <i class="bi bi-keyboard"></i> Accesos
                                </a>
                                </li>
                            </ul>
                            <hr>
                        </div>
					</div>
					<div class="col-md-12 text-center">
                        <a aria-label="Chatear en WhatsApp" href="https://wa.me/+5212229999194" target="_blank" class="btn-social btn-sm btn-whatsapp"><i class="bi bi-whatsapp"></i></a>
                        <a href="https://t.me/linuxitos" target="_blank" class="btn-social btn-sm btn-telegram"><i class="bi bi-telegram"></i></a>
                        <a href="https://www.facebook.com/linuxitos/" target="_blank" class="btn-social btn-sm btn-facebook"><i class="bi bi-facebook"></i></a>
                        <a href="https://www.instagram.com/linuxitos/" target="_blank" class="btn-social btn-sm btn-instagram"><i class="bi bi-instagram"></i></a>
                        <a href="https://twitter.com/linuxitos_" target="_blank" class="btn-social btn-sm btn-twitter"><i class="bi bi-twitter-x"></i></a>
                        <a href="https://www.youtube.com/@linuxitos" target="_blank" class="btn-social btn-sm btn-youtube"><i class="bi bi-youtube"></i></a>
					</div>
				</div>
            </div>
        </div>

        <script>
			$("#mdlSearch").on("shown.bs.modal", function () {
                $(this).find("input").first().focus()
            });

            $(document).ready(function () {
                $(document).on('keyup', '.txtSearch', function () {
                    var param = $(this).val();
                    var datas = JSON.stringify({ search: param });
                    if (param == "") {
						$('#divCnt').html('');
                    } else {
                        $.ajax({
                            type: "POST",
                            url: base_url+'search',
                            data: datas,
                            contentType: "application/json; charset=utf-8",
                            dataType: "json",
                            beforeSend: function () {
                                $("#btnSearch").html('<span class="spinner-border spin-1x" role="status"><span class="visually-hidden"> Loading...</span></span>');
                            },
                            success: function (response) {
                                $('#divCnt').html('');
                                if (response.posts.length <= 0) {
									$('#divCnt').html('<div class="alert alert-secondary text-center" role="alert"><i class="bi bi-info-circle"></i> Sin resultados para la búsqueda.</div>');
                                }else{
                                    $.each(response.posts, function (index, item) {
                                        $('#divCnt').append('<a href="" target="_blank" class="list-group-item list-group-item-action d-flex py-3" aria-current="true">'
                                            + '<i class="bi bi-newspaper icon-2x"></i>'
                                            + '<div class="d-flex w-100 justify-content-between">'
                                            + '<div class="mx-2">'
                                            + '<h6 class="mb-0">' + item.nom_post + '</h6>'
                                            + '<p class="mb-0 opacity-75">Leer más...</p>'
                                            + '</div>'
                                            + '<small class="opacity-50 text-nowrap">1w</small>'
                                            + '</div>'
                                            + '</a>');
                                    });
                                }
                            },
                            complete: function () {
                                $("#btnSearch").html('<i class="bi bi-search"></i>');
                            },
							error: function (xhr, ajaxOptions, thrownError) {
								$('#divCnt').html('<div class="alert alert-danger text-center" role="alert"><i class="bi bi-x-circle"></i> ' + xhr.status + ': Error interno, intenta más tarde.</div>');
							},
                            failure: function (jqXHR, textStatus, errorThrown) {
                                console.log("HTTP Status: " + jqXHR.status + "; Error Text: " + jqXHR.responseText);
								$('#divCnt').html('<div class="alert alert-danger text-center" role="alert"><i class="bi bi-x-circle"></i> ' + jqXHR.responseText +'</div>');
                            }
                        });
                    }
				});
			});
		</script>

		<script type="text/javascript">
			$(document).ready(function() {
				$(window).scroll(function() {
					if ($(this).scrollTop() > 100) {
						$('#scroll').fadeIn();
					} else {
						$('#scroll').fadeOut();
					}
				});
				$('#scroll').click(function() {
					$("html, body").animate({
						scrollTop: 0
					}, 600);
					return false;
				});
			});
			$('.no-cerrar').on('click', function(event) {
				event.stopPropagation();
			});
		</script>
	</body>
</html>
