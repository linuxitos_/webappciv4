<?php
namespace App\Models;
use CodeIgniter\Database\ConnectionInterface;
use CodeIgniter\Model;

class Post extends Model{
	protected $table 			= 'posts';
	protected $primaryKey 		= 'id_post';
	protected $allowedFields 	= ['id_post', 'fc_post', 'act_post', 'nom_post', 'desc_post', 'img_post', 'slug_post', 'type_post', 'user_id'];
	protected $returnType 		= 'object';
	protected $db;
}