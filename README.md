# WebApp - Codeigniter v4

## Descripción

En todo proyecto o desarrollo web, se requieren cosas básicas, como lo son, paginación, buscador, panel lateral para dispositivos móviles, un encabezado.

En éste proyecto se presenta un idea de cómo realizar dichas acciones usando PHP-Codeigniter, MySQL, Ajax y Bootstrap 5.3

## Requerimientos

- XAMPP >= 8.2 (Opcional siempre y se cuente con un servidor PHP)
- MySQL/MariaDB
- PHP >= 8.2 (Requerido)
- Bootstrap 5.3
- Composer

## Configuración

En el proyecto se incluye la base de datos;

- Clonar el proyecto
- Descomprimir el proyecto en htdocs en caso de usar xampp, caso contrario colocar en dónde esté la ruta para publicar en su servidor local
- Archivo webappv1.sql: Ejecutar el script con el gestor de base de datos que se tenga instalado, si se usa XAMPP será phpMyAdmin.

- Cambiar el nombre del archivo de entorno de configuración env => .env : posteriormente configurar los accesos a su servidor de base de datos

- Cambiar de nombre del archivo htaccess => .htaccess

- Ejecutar composer update desde la terminal dentro del proyecto

	```
	composer update
	```

- Abrir el navegador e ir a la ruta [http://localhost/webappv1](http://localhost/webappv1) sin se cambió la ruta del proyecto, entonces se debe utilizar el configurado.

## Capturas

![alt tag](./public/assets/imgs/1.png)
![alt tag](./public/assets/imgs/2.png)
![alt tag](./public/assets/imgs/3.png)
![alt tag](./public/assets/imgs/4.png)


#### Developed By
----------------
 * linuxitos - <contact@linuxitos.com>
